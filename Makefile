apply: plan validate format
	terraform apply plan
	ansible-playbook -i inventories/inventory.aws_ec2.yaml playbooks/deploy.yaml

plan: format validate
	terraform plan -out plan

validate: format
	terraform validate

format:
	terraform fmt

.PHONY: format, validate, plan, apply
