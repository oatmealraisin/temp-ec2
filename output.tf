output "temporary_ip" {
  description = "You can now SSH into the development server. This is the Public IP address. Make sure you have the Staging key."

  value = aws_instance.temporary.public_ip
}
