variable "time_to_live" {
  type        = number
  nullable    = false
  description = "How long should this EC2 stay alive? (seconds)"
}

data "aws_key_pair" "key" {
  key_name           = "conceptual"
  include_public_key = true

  filter {
    name   = "tag:Name"
    values = ["Conceptual Key"]
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
resource "aws_security_group" "temporary" {
  name        = "Temporary Developer SG"
  description = "Allows HTTP/S and SSH access for development."

  vpc_id = data.aws_vpc.default.id

  ingress {
    description      = "SSH from World"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP AUX"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    managed = "True"
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance#argument-reference
resource "aws_instance" "temporary" {
  ami           = "ami-0fec2c2e2017f4e7b"
  instance_type = "t3.large"

  key_name = data.aws_key_pair.key.key_name

  #subnet_id = data.aws_subnet.default.id

  vpc_security_group_ids = [aws_security_group.temporary.id]

  tags = {
    Name    = "Temporary EC2"
    managed = "True"
  }

  volume_tags = {
    managed = "True"
  }

  root_block_device {
    volume_size = 75
  }

  user_data = <<EOF
#!/bin/bash

# https://docs.docker.com/engine/install/debian/#install-using-the-repository
# Move this to ansible
#sudo apt-get update
#sudo apt-get -y install ca-certificates curl gnupg ack make
#
#sudo install -m 0755 -d /etc/apt/keyrings
#curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
#sudo chmod a+r /etc/apt/keyrings/docker.gpg
#
#echo \
#"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
#"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#
#sudo apt-get update
#
#sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
#
#sudo usermod -aG docker admin
sleep ${var.time_to_live}
sudo shutdown
EOF

}

# Gitlab
# # # #

# TODO: Use gitlab_projects data and gitlab_groups to add a temp deploy key to each repo
# TODO: Create script to clone all subprojects of a gitlab group
